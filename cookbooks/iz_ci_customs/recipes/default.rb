template '/home/vagrant/.bash_functions' do
  source 'bash_functions.sh.erb'
  owner 'vagrant'
  mode '0755'
end

template '/home/vagrant/.gitconfig' do
  source 'gitconfig.erb'
  owner 'vagrant'
  mode '0755'
end

template '/home/vagrant/.bash_profile' do
  source 'bash_profile.sh.erb'
  owner 'vagrant'
  mode '0755'
end